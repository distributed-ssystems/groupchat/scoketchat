import socket, threading, datetime, os

# DEFINE CONSTANTS
# HOST = "localhost"
PORT = 6677
BYTES = 1024
ENCDEC = "utf-8"

SERVERHOST = input("ENTER SERVER IP:: \n")
SOCKET_BIND_ADDR = (SERVERHOST, PORT)

# SETTING SOCKET AND USERNAME
clientUserName = input("YOUR USER NAME: ")
chatRoomClientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# CONNECT TO SERVER, IF SERVER IS UNREACHABLE WE EXIT
try:
    chatRoomClientSocket.connect(SOCKET_BIND_ADDR)
except:
    print("UNABLE TO REACH CHAT SERVER")
    exit(1)


# A FUNCTION TO HANDLE GRACEFUL EXIT WHEN THE SERVER DISCONNECTS
def gracefulExit():
    print("ERROR ENCOUNTERED")
    chatRoomClientSocket.shutdown(socket.SHUT_RDWR)
    chatRoomClientSocket.close()
    os._exit(1)


# A FUCTION THAT RECEIVES MESSEGES FROM SERVER AND PRINTS TO THE CLIENT
# IF AN ERROR OCCURS GRACEFULEXIT IS CALLED
def acceptMessages():
    while True:
        try:
            receivedMessage = chatRoomClientSocket.recv(BYTES).decode(ENCDEC)
            if receivedMessage:
                if receivedMessage == "setUserName":
                    chatRoomClientSocket.send(clientUserName.encode(ENCDEC))
                else:
                    print(
                        datetime.datetime.now().strftime("%H:%M:%S")
                        + " "
                        + receivedMessage
                    )
            else:
                break
        except ConnectionError:
            break

    gracefulExit()


# A FUCTION THAT SENDS MESSEGES FROM THE CLIENT
# IF AN ERROR OCCURS GRACEFULEXIT IS CALLED
def sendMessages():
    while True:
        try:
            clientMessage = f'{clientUserName}: {input("")}'
            chatRoomClientSocket.send(clientMessage.encode(ENCDEC))
        except ConnectionError:
            break
    gracefulExit()


# BOTH FUCTIONS RUN IN SEPARATE THREADS TO ENABLE CLIENTS TO SEND/RECEIVE MESSAGE CONCURRENTLY
acceptThread = threading.Thread(target=acceptMessages)
acceptThread.start()

sendThread = threading.Thread(target=sendMessages)
sendThread.start()
