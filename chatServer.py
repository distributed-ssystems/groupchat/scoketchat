import socket, threading, sys
# from multiprocessing import Process

# DEFINING CONSTANTS, HOST IS DEFINED AS BLANK TO ALLOW THE SERVER TO ACCEPT CONNECTION FROM ANY IPADDRESS
HOST = ""
TCPPORT = 6677
SOCKET_ADDR = (HOST, TCPPORT)
BYTES = 1024
ENCDEC = "utf-8"

# DEFINING CONSTANTS (UDP)
UDPPORT = 55555


# DEFINING LISTS TO HOLS LIST OF CLIENTS AND USERNAMES (TCP)
connectedClientsTcp = []
userNamesList = []

# LISTS TO HOLD MESSEGES AND CLIENTS LIST (UDP)
udpMessagesList = []
connectedClientsUdp = []


# DEFINE A TCP SOCKET AND BIND IT
tcpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpServerSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpServerSocket.bind(SOCKET_ADDR)

# DEFINING UDP SERVER SOCKET
udpServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


# A FUCTION THAT PRINTS LOG MESSGES TO THE SERVER'S SCREEN
def serverLogs(logMessage):
    print("")
    print(f"[[[\t {logMessage} \t]]]")


###############################################################################################################################################
# ############################################### TCP  FUCNTIONS ##############################################################################

# A FUNCTIO THAT RMEOVES A CLIENT/USERNAME WHEN CLIENT IS UNREACHABLE
def removeClient(clientConnection):
    clientIndex = connectedClientsTcp.index(clientConnection)
    clientUserName = userNamesList[clientIndex]
    connectedClientsTcp.remove(clientConnection)
    userNamesList.remove(clientUserName)
    clientConnection.shutdown(socket.SHUT_RDWR)
    clientConnection.close()
    serverLogs(clientUserName + " DISCONNECTED!")
    broadcastMessage(clientUserName + " DISCONNECTED!","")


# THIS FUCNTION INFINITELY ACCEPTS INCOMING MESSAGES FROM TCP CLIENTS AND SENDS IT TO ALL CONNECTED CLIENTS
def broadcastMessage(message,sender):
    # SEDNING TO ALL CONECTED TCP CLIENTS
    try:
        for singleClient in connectedClientsTcp:
            if singleClient == sender:
                continue
            else:
                singleClient.send(message.encode(ENCDEC))
            
    except ConnectionError:
        removeClient(singleClient)
    

    # SENDING TO ALL CONNECTED UDP CLIENTS
    try:
        for client in connectedClientsUdp:
            udpServerSocket.sendto(message.encode(), client)
    except Exception as e :
        print(e)


# THIS FUNCTION RUNS IN A SINGLE THREAD AND HANDLES A SINGLE CLIENT
def handleTcpClient(clientSocketConnection, clientAddr):
    while True:
        try:
            recievedMessage = clientSocketConnection.recv(BYTES).decode(ENCDEC)
            if recievedMessage:
                broadcastMessage(recievedMessage, clientSocketConnection)
            else:
                removeClient(clientSocketConnection)
                break
        except ConnectionError:
            removeClient(clientSocketConnection)
            break


# MAIN FUCTION
# SERVER STARTS LISTENING
# ACCEPTS USERNAME
# AND CREATES A THREAD FOR A EACH CLIENT
def startTcpServer():
    tcpServerSocket.listen()
    serverLogs("TCP SERVER RUNNING ...")
 
    while True:
        clientSocketConnection, clientAddr = tcpServerSocket.accept()
        clientSocketConnection.send("setUserName".encode(ENCDEC))
        clientUserName = clientSocketConnection.recv(BYTES).decode(ENCDEC)
        connectedClientsTcp.append(clientSocketConnection)
        userNamesList.append(clientUserName)

        broadcastMessage(clientUserName + " JOINED THE CHAT VIA TCP",clientSocketConnection)

        singleClientThread = threading.Thread(
            target=handleTcpClient, args=(clientSocketConnection, clientAddr)
        )
        singleClientThread.start()

############################################################ END OF TCP FUNCTIONS #################################################
###################################################################################################################################


###################################################################################################################################
################################################################# UDP FUNCTIONS ###################################################



# FUNCTION THAT RECEIVES MESSAGES FROM CLIENTS ANS SAVES IT TO MESSEGES LIST
def receiveMessagesUdp():
    while True:
        try:
            message, clientAddr = udpServerSocket.recvfrom(1024)
            udpMessagesList.append((message, clientAddr))
        except:
            serverLogs("UNABLE TO RECV MSG")


# FUNCTION THAT BROADCASTS THE LAST MESSEGE STORED TO LIST OF CLIENTS
# WE CLEAR THE CONTENTS OF THE MESSEGES LIST AFTER RETRIEVING THE LAST MESSEGE
# IF SENDER IF NEW, IT IS ADDED TO CLIENT'S LIST
# IF MESSEGE STARTS WITH myUserNameIs WE BROADCAST USER JOINED CHAT


def broadcastMessagesUdp():
    
    while True:
       
        while udpMessagesList.__len__() != 0:
            message, clientAddr = udpMessagesList[udpMessagesList.__len__() - 1]
            udpMessagesList.clear()
            if clientAddr not in connectedClientsUdp:
                connectedClientsUdp.append(clientAddr)
            for client in connectedClientsUdp:
                try:
                    if client == clientAddr:
                        continue
                    else:
                        if message.decode().startswith("myUserNameIs"):
                            userName = message.decode().split(":")[1]
                            udpServerSocket.sendto(
                                f"{userName} JOINED THE CHAT VIA UDP".encode(), client
                            )

                        else:
                            udpServerSocket.sendto(message, client)

                except:
                    serverLogs(f"UNABLE TO BROADCAST TO {client}")
                    connectedClientsUdp.remove(clientAddr)
                    

            for singleClient in connectedClientsTcp:
                try:

                        if message.decode().startswith("myUserNameIs"):
                            userName = message.decode().split(":")[1]
                            singleClient.send(f"{userName} JOINED THE CHAT VIA UDP".encode())
                        else:
                            singleClient.send(message)
            
                except ConnectionError:
                    removeClient(singleClient)



# MAINFUNCTION
# SERVER IS BOUND TO THE SPECIFIED HOST AND STARTS LISTENING
# CREATES A THREAD FOR RECEIVING AND BROADCASTING

def startUdpServer():
    udpServerSocket.bind((HOST, UDPPORT))
    serverLogs("UDP SERVER RUNNING...")

    receiveThread = threading.Thread(target=receiveMessagesUdp)
    broadcastThread = threading.Thread(target=broadcastMessagesUdp)

    receiveThread.start()
    broadcastThread.start()
    

################################### END OF UDP FUNCTIONS #################################################
##########################################################################################################


# HERE WE CALL THE MAIN SERVERS
# WE CREATE  SEPARATE THREAD FOR EACH SERVER
# AND EACH SERVER USES MULTITHREADING TO RUN ITS OWN FUNCTIONS

# tcpProcess = Process(target=startTcpServer).start()
# udpProcess = Process(target=startUdpServer).start()

tcpThread = threading.Thread(target=startTcpServer).start()
udpThread = threading.Thread(target=startUdpServer).start()



