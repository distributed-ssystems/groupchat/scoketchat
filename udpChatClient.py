import socket, threading, random, os, datetime

# DEFINING SERVER ADDRESS THAT THE CLIENT SOCKET CONNECTS TO
# serverIP = "localhost"
serverPort = 55555

udpClientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

serverIP = input("ENTER SERVERIP:: \n")
serverAddr = (serverIP, serverPort)

# BINDING EACH CLIENT TO DIFFERENT PORTS GENERATED RANDOMELY
udpClientSocket.bind(("0.0.0.0", random.randint(8000, 9000)))

# ACCEPTING USERNAME FROM CLIENT
userName = input("USER NAME::\n")
udpClientSocket.sendto(f"myUserNameIs:{userName}".encode(), serverAddr)


# A FUNCTION TO HANDLE GRACEFUL EXIT WHEN AN EXCEPTION OCCURS
def gracefulExit():
    print("CONNECTION ERROR ENCOUNTERED")
    udpClientSocket.shutdown(socket.SHUT_RDWR)
    udpClientSocket.close()
    os._exit(1)


# FUNCTION TO RECEIVE MESSEGES FROM THE SERVER (INFINITELY)
# ADDS TCURRENT TIMESTAMP TO RECVD MSG
def receiveMessages():
    try:
        while True:
            try:
                message, addr = udpClientSocket.recvfrom(1024)
                print(
                    datetime.datetime.now().strftime("%H:%M:%S")
                    + " => "
                    + message.decode("utf-8")
                )

            except:
                gracefulExit()

    except KeyboardInterrupt:
        gracefulExit()


# FUCNTION TO SEND MESSEGE FROM CLIENT TO SERVER (INFINITELY)
def sendMessages():
    try:
        while True:
            try:
                messageFromClient = input("")

                udpClientSocket.sendto(
                    f"{userName}: {messageFromClient}".encode(), serverAddr
                )

            except:
                gracefulExit()

    except KeyboardInterrupt:
        gracefulExit()


# EACH FUCTION RUNS IN ITS OWN THREAD , ENABLING THE CLIENT TO SEND AND RECEIVE AT THE SAME TIME
receiveThread = threading.Thread(target=receiveMessages)
receiveThread.start()

sendThread = threading.Thread(target=sendMessages)
sendThread.start()
